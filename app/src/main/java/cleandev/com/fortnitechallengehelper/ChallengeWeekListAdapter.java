package cleandev.com.fortnitechallengehelper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ChallengeWeekListAdapter extends ArrayAdapter<ChallengeSet> {
    Context mContext;

    public ChallengeWeekListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<ChallengeSet> objects) {
        super(context, resource, objects);
        mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        MyViewHolder holder = null;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row= inflater.inflate(R.layout.challenge_week_row,parent,false);
            holder = new MyViewHolder(row);
            row.setTag(holder);

        }else{
            holder = (MyViewHolder) row.getTag();
        }

        ChallengeSet currentSet = getItem(position);

        holder.weekSetTitle.setText(currentSet.getSetTitle());

        return row;

    }

    private class MyViewHolder{

        TextView weekSetTitle;

        MyViewHolder(View v){
           weekSetTitle= v.findViewById(R.id.setTitle);
        }
    }
}
