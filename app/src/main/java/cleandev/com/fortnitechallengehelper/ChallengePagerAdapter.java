package cleandev.com.fortnitechallengehelper;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ChallengePagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragmentList =new ArrayList<>();
    private final List<String> mTitleList = new ArrayList<>();

    public void addFragment(Fragment frag, String title){
        mFragmentList.add(frag);
        mTitleList.add(title);

    }

    public ChallengePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
