package cleandev.com.fortnitechallengehelper;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class ChallengeFragment extends Fragment {
    DatabaseReference ChallengeReference = MainActivity.MyDatabase.getReference().child("seasons/5");
    ListView weekList;
    ArrayList<ChallengeSet> mChallengeSet;
    ChildEventListener mChallengeListener;
    ChallengeWeekListAdapter mWeekAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.challengefragment,container,false);

        mChallengeSet = new ArrayList<>();
        mWeekAdapter = new ChallengeWeekListAdapter(getContext(),R.layout.challenge_week_row,mChallengeSet);
        createChallengeSetListener();

        weekList = v.findViewById(R.id.challengeListView);
        weekList.setAdapter(mWeekAdapter);

        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        ChallengeReference.addChildEventListener(mChallengeListener);
       // Toast.makeText(getContext(),mChallengeSet.get(1).getSetTitle(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        mWeekAdapter.clear();
        ChallengeReference.removeEventListener(mChallengeListener);
    }

    public void createChallengeSetListener(){

        mChallengeListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ChallengeSet newSet = new ChallengeSet();
                newSet.setSetTitle((String)dataSnapshot.child("title").getValue());
                mWeekAdapter.add(newSet);
                System.out.println(dataSnapshot.getKey()+"\n\n\n\n\n\n");
                System.out.println((String)dataSnapshot.child("title").getValue());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

    }
}
