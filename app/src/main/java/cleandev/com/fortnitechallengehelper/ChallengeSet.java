package cleandev.com.fortnitechallengehelper;

public class ChallengeSet {

    String setTitle;

    public String getSetTitle() {
        return setTitle;
    }

    public void setSetTitle(String setTitle) {
        this.setTitle = setTitle;
    }
}
