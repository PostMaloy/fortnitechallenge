package cleandev.com.fortnitechallengehelper;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.View;

import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    public static FirebaseDatabase MyDatabase = FirebaseDatabase.getInstance();

    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewPager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tabLayout);

        setupViewPager(mViewPager);

        mTabLayout.setupWithViewPager(mViewPager);




    }


    //creates the adapter and adds the fragments we want to the TabLayout
    public void setupViewPager(ViewPager vp){
        ChallengePagerAdapter pagerAdapter = new ChallengePagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(new ChallengeFragment(),"Challenges");

        vp.setAdapter(pagerAdapter);
    }
}
